# Food rating based on ingredients.


One of the Largest Restaurant chain in the world is interested in making a variety of new
dishes for their customers. They want to understand which of the food items most of the food
lovers are loves to eat.

The provided dataset contains information about food items with the ingredients used in it,
along with the Customer rating(on the scale of 0 to 10).

In this Challenge, You have to predict the Rating of a FoodItem given the ingredients used in
it. This helps the Company to try different dishes with different ingredients so that they can
attract the new Customers/they can maintain their retention of Customers which improves their
business.

Note: As the company is very much confidential about the names of ingredients used.All
independent attributes can be treated as different ingredients.